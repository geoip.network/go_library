package geoip_network

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"time"
)

type ClientType byte

const (
	NoAuth ClientType = iota
	Bearer
	Dynamic
)

// The Client represents the User credentials and essential API interface.
// While lightweight, it is more efficient to instantiate it once and share it across your application.
type Client struct {
	Type          ClientType
	BaseURL       *url.URL
	UserAgent     string
	Authorization string
	Expires       time.Time
	RefreshKey    string
	Cookies       []http.Cookie
	HttpClient    *http.Client
}

type loginRespBody struct {
	Refresh string `json:"refresh"`
	TTL     int64  `json:"ttl"`
}

func (c *Client) newRequest(method string, path string, body interface{}) (*http.Request, error) {
	rel := &url.URL{Path: path}
	u := c.BaseURL.ResolveReference(rel)
	var buf io.ReadWriter
	if body != nil {
		buf = new(bytes.Buffer)
		err := json.NewEncoder(buf).Encode(body)
		if err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, u.String(), buf)
	if err != nil {
		return nil, err
	}
	if body != nil {
		req.Header.Set("Content-Type", "application/json")
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	switch c.Type {
	case Bearer:
		req.Header.Set("Authorization", c.Authorization)
	case Dynamic:
		if c.Expires.Before(time.Now()) {
			err := c.login()
			if err != nil {
				return nil, err
			}
		} else if c.Expires.Before(time.Now().Add(5 * time.Second)) {
			err := c.refresh()
			if err != nil {
				return nil, err
			}
		}
		for _, cookie := range c.Cookies {
			req.AddCookie(&cookie)
		}
	}
	return req, nil
}

func (c *Client) do(req *http.Request, v interface{}) (*http.Response, error) {
	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	err = json.NewDecoder(resp.Body).Decode(v)
	return resp, err
}

func (c *Client) login() error {
	var buf io.ReadWriter
	req, err := http.NewRequest("POST", "https://auth.geoip.network/v1.0/login", buf)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	req.Header.Set("Authorization", c.Authorization)
	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	var jdata loginRespBody
	err = json.NewDecoder(resp.Body).Decode(&jdata)
	if err != nil {
		return err
	}
	if resp.StatusCode == 401 {
		fmt.Println("WARNING: Not Authenticated rate limits may apply")
	}
	c.Expires = time.Now().Add(time.Duration(jdata.TTL) * time.Second)
	c.RefreshKey = jdata.Refresh
	return nil
}

func (c *Client) refresh() error {
	var buf io.ReadWriter
	req, err := http.NewRequest("POST", "https://auth.geoip.network/v1.0/login", buf)
	if err != nil {
		return err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", c.UserAgent)
	req.Header.Set("X-RENEW-KEY", c.RefreshKey)
	for _, cookie := range c.Cookies {
		req.AddCookie(&cookie)
	}
	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode == 401 {
		fmt.Println("WARNING: Not Authenticated rate limits may apply")
	}
	defer resp.Body.Close()
	var jdata loginRespBody
	err = json.NewDecoder(resp.Body).Decode(&jdata)
	if err != nil {
		return err
	}
	c.Expires = time.Now().Add(time.Duration(jdata.TTL) * time.Second)
	c.RefreshKey = jdata.Refresh
	return nil
}
