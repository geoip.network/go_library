package geoip_network

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"net/url"
	"time"
)

// GeoJSONGeometry represents the GeoJSON geometry data returned by the API.
// The Coordinates field is presented in LongLat format as per the GeoJSON standard [RFC 7946](https://datatracker.ietf.org/doc/html/rfc7946)
type GeoJSONGeometry struct {
	// Type contains the GeoJSON object type - This will always be "Point"
	Type string `json:"type"`
	// Coordinates is an Array containing the coordinates of the point in LongLat format
	Coordinates []float64 `json:"coordinates"`
}

// GeoJSONProperties represents the GeoJSON properties data returned by the API.
// A value of -1.0 means that the data is only accurate for the country, any value >0 indicates the confidence radius (in meters)
type GeoJSONProperties struct {
	// Radius is either -1 (meaning country level accuracy only) or the accuracy radius of the estimated location (in meters).
	Radius float64 `json:"radius"`
}

// GeoJSON represents the GeoJSON data returned by the API.
type GeoJSON struct {
	// Type contains the GeoJSON object type - This will always be "Feature"
	Type string `json:"type"`
	// Geometry contains the GeoJSON geometry data, this will always define a "Point".
	Geometry GeoJSONGeometry `json:"geometry"`
	// Properties is a "free field" as defined by the GeoJson standard, in this API we use it to hold the accuracy "Radius" field that is otherwise undefined by GeoJSON
	Properties GeoJSONProperties `json:"Properties"`
}

type resultRaw struct {
	Error       string  `json:"error"`
	CountryCode string  `json:"allocated_cc"`
	ASName      string  `json:"as-name"`
	ASN         string  `json:"asn"`
	CIDR        string  `json:"cidr"`
	RIR         string  `json:"rir"`
	Geo         GeoJSON `json:"geo"`
	Timestamp   uint64  `json:"timestamp"`
}

// The Result type provides access to the returned data in a native format.
// Results are found via Longest Prefix Matching, where the results from the covering prefix that is closest to the requested range / ip is provided.
// If the IP address was not found in the API the Error field will contain the reason.
// Otherwise, the data fields will be populated.
type Result struct {
	// Error, if populated, provides the reason that a lookup failed (most-likely "no covering prefix found").
	Error string `json:"error"`
	// CountryCode provides the ISO Country Code that the IP Address block is registered to.
	CountryCode string `json:"allocated_cc"`
	// ASName provides the name the the ISP has allocated to the Network that in announcing this IP Address block via BGP.
	ASName string `json:"as-name"`
	// ASN provides the Autonomous System Number (unique ID) of the network that is announcing this IP Address block via BGP.
	ASN string `json:"asn"`
	// CIDR provides the CIDR of the closest prefix match to the requested IP or CIDR.
	CIDR string `json:"cidr"`
	// RIR provides the name of the Regional Internet Registry that the IP Address block is managed by
	RIR string `json:"rir"`
	// Geo provides a representation of Estimated location of the IP Address block as a GeoJSON object
	Geo GeoJSON `json:"geo"`
	// Timestamp provides the timestamp that the latest estimation of the location of the IP Address Block was completed by.
	Timestamp time.Time `json:"timestamp"`
}

// NewBearerClient creates and instance of Client that injects the supplied Bearer into requests.
func NewBearerClient(httpClient *http.Client, userAgent string, token string) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := &Client{
		Type:       Bearer,
		HttpClient: httpClient,
		BaseURL: &url.URL{
			Scheme: "https",
			Host:   "api.geoip.network",
			Path:   "v1.0/",
		},
	}
	c.UserAgent = userAgent
	c.Authorization = "Bearer " + token
	return c
}

// NewBearerClient creates and instance of Client that handles login and credential renewal for the "Dynamic"/SessionCookie authentication method
func NewDynamicClient(httpClient *http.Client, userAgent string, username string, password string) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := &Client{
		Type:       Dynamic,
		HttpClient: httpClient,
		BaseURL: &url.URL{
			Scheme: "https",
			Host:   "api.geoip.network",
			Path:   "v1.0/",
		},
	}
	c.UserAgent = userAgent
	encodedString := base64.StdEncoding.EncodeToString([]byte(username + ":" + password))
	c.Authorization = "Basic " + encodedString
	c.Expires = time.Now()
	return c
}

func newClient(httpClient *http.Client, userAgent string) *Client {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := &Client{
		HttpClient: httpClient,
		BaseURL: &url.URL{
			Scheme: "https",
			Host:   "api.geoip.network",
			Path:   "v1.0/",
		},
	}
	c.UserAgent = userAgent
	return c
}

// LookupIP provides an easy interface to the free / unauthenticated API.
// It makes a GET request to the https://api.geoip.network/v1.0/cidr/{ipaddr} and returns either a Result struct or an error.
func LookupIP(ipAddr string, httpClient *http.Client, userAgent string) (*Result, error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := newClient(httpClient, userAgent)
	return c.LookupIP(ipAddr)
}

// LookupCIDR provides an easy interface to the free / unauthenticated API.
// It makes a GET request to the https://api.geoip.network/v1.0/cidr/{cidr} and returns either a Result struct or an error.
func LookupCIDR(cidr string, httpClient *http.Client, userAgent string) (*Result, error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := newClient(httpClient, userAgent)
	return c.LookupCIDR(cidr)
}

// LookupBulk provides an easy interface to the free / unauthenticated bulk API.
// It makes a POST request containing the list of IPs (and/or CIDRs) as a JSON body to the https://api.geoip.network/v1.0/cidrs endpoint
// and returns either an array of Result structs or an error.
// Note: If an IP or CIDR in the list could not be found it will not raise a general error, but will have a non-empty Result.Error field.
func LookupBulk(ipsAndCIDRs []string, httpClient *http.Client, userAgent string) ([]Result, error) {
	if httpClient == nil {
		httpClient = http.DefaultClient
	}
	c := newClient(httpClient, userAgent)
	return c.LookupBulk(ipsAndCIDRs)
}

// LookupIP provides an easy interface to the API and injects any relevant Authentication as provided by the Client instance.
// It makes a GET request to the https://api.geoip.network/v1.0/cidr/{ipaddr} and returns either a Result struct or an error.
func (c *Client) LookupIP(ipAddr string) (*Result, error) {
	return c.LookupCIDR(ipAddr)
}

// LookupCIDR provides an easy interface to the API and injects any relevant Authentication as provided by the Client instance.
// It makes a GET request to the https://api.geoip.network/v1.0/cidr/{cidr} and returns either a Result struct or an error.
func (c *Client) LookupCIDR(cidr string) (*Result, error) {
	req, err := c.newRequest("GET", "cidr/"+cidr, nil)
	if err != nil {
		return nil, err
	}
	var raw resultRaw
	resp, err := c.do(req, &raw)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == 401 {
		fmt.Println("WARNING: Not Authenticated rate limits may apply")
	}
	result := parseRawResult(&raw)
	return &result, nil
}

// LookupBulk provides an easy interface to the API and injects any relevant Authentication as provided by the Client instance.
// It makes a POST request containing the list of IPs (and/or CIDRs) as a JSON body to the https://api.geoip.network/v1.0/cidrs endpoint
// and returns either an array of Result structs or an error.
// Note: If an IP or CIDR in the list could not be found it will not raise a general error, but will have a non-empty Result.Error field.
func (c *Client) LookupBulk(ipsAndCIDRs []string) ([]Result, error) {
	req, err := c.newRequest("POST", "/cidrs", ipsAndCIDRs)
	if err != nil {
		return nil, err
	}
	var raws []resultRaw
	resp, err := c.do(req, &raws)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == 401 {
		fmt.Println("WARNING: Not Authenticated rate limits may apply")
	}
	var results []Result
	for _, raw := range raws {
		result := parseRawResult(&raw)
		results = append(results, result)
	}
	return results, nil
}

func parseRawResult(raw *resultRaw) Result {
	result := Result{
		raw.Error,
		raw.CountryCode,
		raw.ASName,
		raw.ASN,
		raw.CIDR,
		raw.RIR,
		raw.Geo,
		time.Unix(int64(raw.Timestamp)*int64(time.Second), 0),
	}
	return result
}
