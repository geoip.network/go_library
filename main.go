package main

import (
	"fmt"
	"gitlab.com/geoip.network/go_library/pkg/geoip_network"
	"os"
)

func main() {
	result, err := geoip_network.LookupCIDR("89.39.136.0/24", nil, "Tim-Test")
	if err != nil {
		if result != nil {
			fmt.Println(result.Error)
		}
		panic(err)
	}
	if result.Error != "" {
		fmt.Println(result.Error)
		os.Exit(1)
	}
	fmt.Println("89.39.136.0/24:", result.CountryCode, "Coordinates(Lat,Long):", result.Geo.Geometry.Coordinates[1], ",", result.Geo.Geometry.Coordinates[0])
}
