# GeoIP.Network Go Library
[![GoDoc](https://godoc.org/gitlab.com/geoip.network/go_library/?status.svg)](https://pkg.go.dev/gitlab.com/geoip.network/go_library@v0.2.0/pkg/geoip_network)
![Release Badge](https://gitlab.com/geoip.network/go_library/-/badges/release.svg)
![Pipeline Badge](https://gitlab.com/geoip.network/go_library/badges/main/pipeline.svg)

The official Go interface to GeoIP.Network

![Screenshot of example code from main.go](screenshots/screenshot-1.png)

Localize IP addresses instantly anywhere in the world.

Improve your customer’s experience and optimize your marketing by using GeoIP.Network to discover your client’s location in real-time.

Our API is free to use for up to 10,000 requests per day (more than most commercial projects offer per month in their paid packages) - or unlimited if you sponsor the project for the cost of a cup of coffee per month.
Discover details like ISP, Country, and Location instantly.
GeoIP.Network is a Not-For-Profit (and open-source) project that aims to provide reliable and accurate IP localization data for free to everyone. Building on research done at world leading universities, we use a blend of information from the Internet Routing Registry (IRR), live BGP streams, and a stochastic-progressive latency measurement algorithm to provide the most up-to-date and accurate data possible.

TLDR; We use science and public data and offer accurate GeoIP data for free.

## Usage (free < 10000 requests / day):

___NOTE: The following IP addresses are Documentation addresses. As such you will need to replace them with valid public IP addresses for these examples to work.___

```go
// Single IP
result, err := geoip_network.LookupIP("198.51.100.1", nil, "Tim-Test")
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
fmt.Println(result.CIDR, result.CountryCode, "Coordinates(Lat,Long):", result.Geo.Geometry.Coordinates[1], ",", result.Geo.Geometry.Coordinates[0])
// 198.51.100.0/24 US Coordinates(Lat,Long): 45.7364 , -112.4042


// IP Range (CIDR)
result, err := geoip_network.LookupIP("198.51.100.0/24", nil, "Tim-Test")
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
fmt.Println(result.CIDR, result.CountryCode, "Coordinates(Lat,Long):", result.Geo.Geometry.Coordinates[1], ",", result.Geo.Geometry.Coordinates[0])
// 198.51.100.0/24 US Coordinates(Lat,Long): 45.7364 , -112.4042


// Bulk lookup
for _,result := range geoip_network.LookupBulk(["2001:db8::/48", "198.51.100.1", "0.0.0.0/24"]){
	...
}
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "2001:db8::/32", "geo": {"geometry": {"coordinates": [16.72425629, 62.88018421], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1634593342}, 
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
// {"error": "no covering prefix found"}
```

## Usage (sponsor):
___NOTE: The following IP addresses are Documentation addresses. As such you will need to replace them with valid public IP addresses for these examples to work.___
```go
// Login
geoip := geoip_network.NewBearerClient(nil, "Tim-Test", "QW4gRXhhbXBsZSBCZWFyZXIgVG9rZW4gZm9yIEdlb0lQLk5ldHdvcms=")

// Single IP
result, err := geoip.LookupIP("198.51.100.1")
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
fmt.Println(result.CIDR, result.CountryCode, "Coordinates(Lat,Long):", result.Geo.Geometry.Coordinates[1], ",", result.Geo.Geometry.Coordinates[0])
// 198.51.100.0/24 US Coordinates(Lat,Long): 45.7364 , -112.4042


// IP Range (CIDR)
result, err := geoip.LookupIP("198.51.100.0/24")
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
fmt.Println(result.CIDR, result.CountryCode, "Coordinates(Lat,Long):", result.Geo.Geometry.Coordinates[1], ",", result.Geo.Geometry.Coordinates[0])
// 198.51.100.0/24 US Coordinates(Lat,Long): 45.7364 , -112.4042


// Bulk lookup
for _,result := range geoip.LookupBulk(["2001:db8::/48", "198.51.100.1", "0.0.0.0/24"]){
...
}
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "2001:db8::/32", "geo": {"geometry": {"coordinates": [16.72425629, 62.88018421], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1634593342}, 
// {"allocated_cc": "--", "as-name": "Documentation ASN", "asn": "AS64496", "cidr": "198.51.100.0/24", "geo": {"geometry": {"coordinates": [-112.404207, 45.73643438], "type": "Point"}, "properties": {"radius": 0.0}, "type": "Feature"}, "rir": "IANA", "timestamp": 1643422555},
// {"error": "no covering prefix found"}
```
